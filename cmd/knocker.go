package cmd

import (
	"strings"
	"time"

	"golang.org/x/crypto/ssh"
)

type knocker struct {
	stopChan    chan struct{}
	hostChan    chan string
	credentials []*credentials
	timeout     time.Duration
}

// NewKnocker creates and returns knocker instance
func NewKnocker(stopChan chan struct{}, hostChan chan string, creds []*credentials, t time.Duration) *knocker {
	return &knocker{
		stopChan:    stopChan,
		hostChan:    hostChan,
		credentials: creds,
		timeout:     t,
	}
}

func (k *knocker) run(resultChan chan string) {
	for {
		select {
		case host, ok := <-k.hostChan:
			if len(host) > 0 {
				resultChan <- k.knock(host)
			}
			if !ok {
				// channel closed
				return
			}
		case <-k.stopChan:
			// we are finished - quit here
			return
		}
	}
}

func (k *knocker) knock(host string) string {
	results := make([]string, len(k.credentials))
	for i, creds := range k.credentials {
		err := connectToHost(creds.user, creds.pass, host, k.timeout)
		if err != nil {
			results[i] = "failed"
		} else {
			results[i] = "connected"
		}
	}
	return host + ":" + strings.Join(results, ";") + "\n"
}

func connectToHost(user, pass, host string, timeout time.Duration) error {
	sshConfig := &ssh.ClientConfig{
		User:    user,
		Auth:    []ssh.AuthMethod{ssh.Password(pass)},
		Timeout: timeout,
	}
	sshConfig.HostKeyCallback = ssh.InsecureIgnoreHostKey()

	client, err := ssh.Dial("tcp", host, sshConfig)
	if err != nil {
		return err
	}
	client.Close()
	return nil
}
