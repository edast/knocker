package cmd

import (
	"bufio"
	"fmt"
	"io"
	"os"
	"strings"
)

const _credSep = ":"

type credentials struct {
	user string
	pass string
}

func ReadCreadentials(input io.Reader) []*credentials {
	defer func() {
		if f, ok := input.(*os.File); ok {
			f.Close()
		}
	}()
	s := bufio.NewScanner(input)
	result := make([]*credentials, 0)
	for s.Scan() {
		line := s.Text()
		if len(line) == 0 {
			continue
		}
		creds := strings.SplitN(line, _credSep, 2)
		if len(creds) != 2 {
			fmt.Printf("bad credentials string `%s`", line)
			continue
		}
		result = append(result, &credentials{user: creds[0], pass: creds[1]})
	}
	return result
}

func ReadHosts(input io.Reader, hostChan chan string, stopChan chan struct{}) {
	defer func() {
		if f, ok := input.(*os.File); ok {
			f.Close()
		}
	}()
	s := bufio.NewScanner(input)
	for s.Scan() {
		host := s.Text()
		select {
		case hostChan <- host:
			// writing host into channel for knocker(s)
		case <-stopChan:
		}
	}
}
