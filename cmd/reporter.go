package cmd

import (
	"fmt"
	"io"
)

type reporter struct {
	reportChan chan string
	stopChan   chan struct{}
	out        io.Writer
}

// NewReporter returns reporter instance
func NewReporter(reportChan chan string, stopChan chan struct{}, out io.Writer) *reporter {
	return &reporter{
		reportChan: reportChan,
		stopChan:   stopChan,
		out:        out,
	}
}

type stringWriter interface {
	WriteString(string) (int, error)
	Flush() error
}

func (r *reporter) run() {
	defer func() {
		if sw, ok := r.out.(stringWriter); ok {
			if err := sw.Flush(); err != nil {
				fmt.Println("Failed writing to output", err)
			}
		}
	}()

	for {
		select {
		case report := <-r.reportChan:
			if sw, ok := r.out.(stringWriter); ok {
				_, err := sw.WriteString(report)
				if err != nil {
					fmt.Println("failed to write to output", err)
				}
				continue
			}
			r.out.Write([]byte(report))
		case <-r.stopChan:
			return
		}
	}
}
