// Copyright © 2018 NAME HERE <EMAIL ADDRESS>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package cmd

import (
	"bufio"
	"fmt"
	"os"
	"sync"
	"time"

	"github.com/spf13/cobra"
)

var cfgFile string
var hostFile string
var credentialsFile string
var numOfWorkers int
var timeout time.Duration
var reportFile string

// rootCmd represents the base command when called without any subcommands
var rootCmd = &cobra.Command{
	Use:   "knocker",
	Short: "Knock on every host in a list",
	Long: `knocker takes list of ips and list of user:pass combos
and tries it on every host in the list. You can control how many
hosts in parallel should be tried. `,
	Run: runKnocker,
}

func check(e error) {
	if e != nil {
		panic(e)
	}
}

func isError(err error) bool {
	if err != nil {
		fmt.Println(err.Error())
	}

	return (err != nil)
}

func runKnocker(cmd *cobra.Command, args []string) {
	var err error
	outWriter := os.Stdout
	if len(reportFile) > 0 {

		outWriter, err = os.OpenFile(reportFile, os.O_RDWR|os.O_TRUNC|os.O_CREATE, 0660)
		if isError(err) {
			fmt.Println("cannot open file", err)
			return
		}
		defer outWriter.Close()
	}
	out := bufio.NewWriter(outWriter)

	reportChan := make(chan string)
	stopChan := make(chan struct{})
	reporter := NewReporter(reportChan, stopChan, out)
	hostChan := make(chan string)
	done := make(chan struct{})

	if len(credentialsFile) == 0 {
		fmt.Println("please provide credentials file")
		cmd.Usage()
		return
	}
	f, err := os.Open(credentialsFile)
	check(err)

	if len(credentialsFile) == 0 {
		fmt.Println("please provide hosts file")
		cmd.Usage()
		return
	}
	f2, err2 := os.Open(hostFile)
	check(err2)

	creds := ReadCreadentials(f)

	go func() {
		reporter.run()
		done <- struct{}{}
	}()

	go func() {
		ReadHosts(f2, hostChan, stopChan)
		close(hostChan)
	}()

	var wg sync.WaitGroup

	for i := 0; i < numOfWorkers; i++ {
		wg.Add(1)
		k := NewKnocker(stopChan, hostChan, creds, timeout)
		go func(knock *knocker) {
			defer wg.Done()
			knock.run(reportChan)
		}(k)
	}

	wg.Wait()
	close(stopChan)
	<-done
}

// Execute adds all child commands to the root command and sets flags appropriately.
func Execute() {
	if err := rootCmd.Execute(); err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
}

func init() {
	rootCmd.Flags().StringVarP(&hostFile, "ips", "i", "", "host file containing ip addresses (one per line)")
	rootCmd.Flags().StringVarP(&credentialsFile, "credentials", "c", "", "credentials file containing user:pass combo (one per line)")
	rootCmd.Flags().StringVarP(&reportFile, "out", "o", "", "output file to write results to. Defaults to stdout")
	rootCmd.Flags().DurationVarP(&timeout, "timeout", "t", 1*time.Second, "timeout for ssh connect")
	rootCmd.Flags().IntVarP(&numOfWorkers, "parallel", "p", 10, "parallel workers to run")
}
