# README #

knocker - utility for checking servers against known credentials

### build ###

```
go build -o knocker main.go
```

### execute ###

```
./knocker -c sample/test_creds.txt -i sample/test_hosts.txt -p 20 -t 500ms
```

execute ```./knocker -h``` for usage
